from datetime import datetime
from flask import Flask, render_template, request, redirect, url_for, json
from random import random
from random import uniform
from math import sin
from math import pi
intervals = [10,100,1000,10000,100000,1000000,10000000]
app = Flask(__name__)


@app.route('/')
def index():
   #print('Request for index page received')
   return 'Hello'

@app.route('/<v1>/<v2>')
def calculate(v1,v2):
        result = ""
        results = []
        for N in intervals:
                #eg N=1000000
            count=0
            for i in range(N):
                point=(uniform(float(v1),float(v2)), random())
                if point[1] < sin(point[0]):
                    count+=1
                        
            answer=(float(count)/float(N))*pi
            results.append(answer)
            result=result+"<br> for "+str(N)+":"+str(answer)
            
        response = app.response_class(
            response=json.dumps(results),
            mimetype='application/json'
        )   
        return response



if __name__ == '__main__':
   app.run()