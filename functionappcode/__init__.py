import logging
from random import random
from random import uniform
from math import sin
from math import pi
intervals = [10,100,1000,10000,100000,1000000,10000000]

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    v1 = req.params.get('v1')
    v2 = req.params.get('v2')
    if not v1:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            v1 = req_body.get('v1')

    if not v2:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            v2 = req_body.get('v2')

    if v1 and v2:
        result = ""
        results = []
        for N in intervals:
                #eg N=1000000
            count=0
            for i in range(N):
                point=(uniform(float(v1),float(v2)), random())
                if point[1] < sin(point[0]):
                    count+=1
                        
            answer=(float(count)/float(N))*pi
            results.append(answer)
            result=result+"<br> for "+str(N)+":"+str(answer)
        return func.HttpResponse(result)
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass a v1 and v2 in the query string or in the request body for a personalized response.",
             status_code=200
        )
